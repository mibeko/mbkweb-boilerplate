set :sitename, '{{siteslug}}'

set :repo_url, "git@github.com:mibeko-web/#{fetch(:sitename)}.git"

set :deploy_to, -> { "/srv/www/#{fetch(:sitename)}" }

set :log_level, :info

set :keep_releases, 2

set :linked_files, fetch(:linked_files, []).push('.env')
set :linked_dirs, fetch(:linked_dirs, []).push('web/app/uploads')

# wpcli
set :wpcli_local_url, "#{fetch(:sitename)}.test"
set :local_tmp_dir, 'tmp'
set :wpcli_local_db_backup_dir, 'db-backups'
set :wpcli_local_uploads_dir, 'web/app/uploads/'

# ssh
set :ssh_options, {
  forward_agent: true,
}

namespace :deploy do
  desc 'Update WordPress template root paths to point to the new release'
  task :update_option_paths do
    on roles(:app) do
      within fetch(:release_path) do
        if test :wp, :core, 'is-installed'
          [:stylesheet_root, :template_root].each do |option|
            value = capture :wp, :option, :get, option, raise_on_non_zero_exit: false
            if value != '' && value != '/themes'
              execute :wp, :option, :set, option, fetch(:release_path).join('web/wp/wp-content/themes')
            end
          end
        end
      end
    end
  end
end
after 'deploy:publishing', 'deploy:update_option_paths'

namespace :deploy do
  desc 'Upload corresponding .env to shared dir'
  task :upload_env do
    local_env, remote_env = ".env.#{fetch(:stage)}", "#{shared_path}/.env"
    on roles(:all) do |host|
      upload! local_env, remote_env
    end
  end
end
after 'deploy:check:directories', 'deploy:upload_env'

# site packages
namespace :deploy do
  desc 'Composer intall for site'
  task :composer_install_site do
    on roles(:app) do
      execute "cd #{fetch(:release_path)} && composer install -o --no-dev"
    end
  end
end
after 'deploy:update_option_paths', 'deploy:composer_install_site'

# mibeko-plug packages
namespace :deploy do
  desc 'Composer intall for mibeko-plug'
  task :composer_install_mibeko_plug do
    on roles(:app) do
      execute "cd #{fetch(:release_path)}/web/app/mu-plugins/mibeko-plug && composer install -o --no-dev"
    end
  end
end
after 'deploy:composer_install_site', 'deploy:composer_install_mibeko_plug'

# theme packages
namespace :deploy do
  desc 'Composer intall for theme'
  task :composer_install_theme do
    on roles(:app) do
      execute "cd #{fetch(:release_path)}/web/app/themes/#{fetch(:sitename)} && composer install -o --no-dev"
    end
  end
end
after 'deploy:composer_install_mibeko_plug', 'deploy:composer_install_theme'

# theme dist
namespace :deploy do
  desc 'Upload generated theme dist folder remote'
  task :upload_dist do
    local_dist = "web/app/themes/#{fetch(:sitename)}/public/"
    remote_destination = "#{release_path}/web/app/themes/#{fetch(:sitename)}"
    run_locally do
      execute "cd #{local_dist} && yarn && yarn build:production"
    end
    on roles(:all) do |host|
      upload! local_dist, remote_destination, recursive: true
    end
  end
end
after 'deploy:composer_install_theme', 'deploy:upload_dist'
