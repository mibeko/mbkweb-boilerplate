set :stage, :staging
set :branch, :development

# Extended Server Syntax
# ======================
server '139.162.56.142', user: "#{fetch(:sitename)}mbk", roles: %w{web app db}

set :deploy_to, -> { "/sites/#{fetch(:wpcli_remote_url)}/files" }

# wpcli
set :wpcli_remote_url, "#{fetch(:sitename)}.mibeko.io"
set :wpcli_remote_uploads_dir, "#{shared_path}/web/app/uploads/"

fetch(:default_env).merge!(wp_env: :staging)
