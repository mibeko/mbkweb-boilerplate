<?php
/**
 * Plugin Name: Disable Posts
 * Plugin URI: https://mibeko.io/
 * Description: Disables posts
 * Version: 1.0
 * Author: Mibeko
 * Author URI: https://mibeko.io/
 * License: MIT
 */

add_action('admin_menu', function () {
    remove_menu_page('edit.php');
});

add_action('wp_before_admin_bar_render', function () {
    global $wp_admin_bar;
    if ($wp_admin_bar) {
        $wp_admin_bar->remove_menu('new-post');
    }
});

add_action('wp_dashboard_setup', function () {
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
}, 10);
