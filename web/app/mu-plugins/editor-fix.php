<?php
/**
 * Plugin Name: Editor Fix
 * Plugin URI: https://mibeko.io/
 * Description: Fixes that interesting fullscreen editor "feature"
 * Version: 1.0
 * Author: Mibeko
 * Author URI: https://mibeko.io/
 * License: MIT
 */

add_action('enqueue_block_editor_assets', function () {
    if (is_admin()) {
        $script = "jQuery( window ).load(function() { const isFullscreenMode = wp.data.select( 'core/edit-post' ).isFeatureActive( 'fullscreenMode' ); if ( isFullscreenMode ) { wp.data.dispatch( 'core/edit-post' ).toggleFeature( 'fullscreenMode' ); } });";
        wp_add_inline_script('wp-blocks', $script);
    }
});
