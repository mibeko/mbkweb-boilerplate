<?php

namespace MWD\ExampleCPT;

// Set up plugin class
class Init
{
    public function __construct()
    {
        // Include all post types
        foreach (glob(MBK_PLUGIN_DIR . 'app/types/*.php') as $filename) {
            include $filename;
        }
    }
}
