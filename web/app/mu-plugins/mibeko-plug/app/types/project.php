<?php

use PostTypes\PostType;
use PostTypes\Taxonomy;

/** Project CPT */
$options = [
  'capability_type' => 'post',
  'has_archive' => false, //idk
  'hierarchical' => false,
  'rewrite' => [
    'slug' => '/work',
  ],
  'show_in_rest' => false,
  'supports' => [ 'title', 'editor', 'page-attributes', 'thumbnail' ],
];

$project = new PostType('project', $options);
$project->taxonomy('sort');
$project->icon('dashicons-hammer');
$project->register();

/** Sort Taxonomy */
$sort = new Taxonomy('sort');
$sort->options( [
  'hierarchical' => true,
  'show_in_rest' => false,
] );
$sort->register();
