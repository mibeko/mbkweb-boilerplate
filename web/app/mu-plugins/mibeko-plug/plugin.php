<?php
/*
Plugin Name: Mibeko Plug
Plugin URI: https://mibeko.io/
Description: Mibeko plugin code, may vary per site
Version: 1.0
Author: Mibeko
Author URI: https://mibeko.io/
License: MIT
*/

namespace MWD\ExampleCPT;


//Set up autoloader
require __DIR__ . '/vendor/autoload.php';

//Define Constants
define( 'MBK_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'MBK_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

// Autoload the Init class
$example_init = new Init();
