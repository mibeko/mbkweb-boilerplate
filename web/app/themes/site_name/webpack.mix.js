const mix = require('laravel-mix');
require('@tinypixelco/laravel-mix-wp-blocks');

const protocol = 'https://';
const url = '{{siteslug}}.lndo.site';
const themeUrl = 'theme-' + url;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Sage application. By default, we are compiling the Sass file
 | for your application, as well as bundling up your JS files.
 |
 */

mix
  .setPublicPath('./public')
  .browserSync({
    open: false,
    proxy: {
      target: 'http://appserver_nginx',
      proxyReq: [
        function(proxyReq) {
          proxyReq.setHeader('host', url);
        },
      ],
    },
    rewriteRules: [
      {
        match: new RegExp(protocol + url, 'g'),
        replace: protocol + themeUrl,
      },
    ],
    socket: {
      domain: protocol + themeUrl,
      port: 80,
    },
  });

mix
  .sass('resources/styles/app.scss', 'styles')
  .sass('resources/styles/editor.scss', 'styles')
  .options({
    processCssUrls: false,
    postCss: [require('tailwindcss')],
  });

mix
  .js('resources/scripts/app.js', 'scripts')
  .js('resources/scripts/customizer.js', 'scripts')
  .blocks('resources/scripts/editor.js', 'scripts')
  .autoload({ jquery: ['$', 'window.jQuery'] })
  .extract();

mix
  .copyDirectory('resources/images', 'public/images')
  .copyDirectory('resources/fonts', 'public/fonts');

mix
  .sourceMaps()
  .version();
