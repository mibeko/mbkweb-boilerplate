@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @noposts
    <x-alert type="warning">
      {!! __('Sorry, but the page you are trying to view does not exist.', '{{siteslug}}') !!}
    </x-alert>

    {!! get_search_form(false) !!}
  @endnoposts
@endsection
