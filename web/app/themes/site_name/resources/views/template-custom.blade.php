{{--
  Template Name: Custom Template
--}}

@extends('layouts.app')

@section('content')
  @hasposts
    @posts
      @include('partials.page-header')
      @include('partials.content-page')
    @endposts
  @endhasposts
@endsection
