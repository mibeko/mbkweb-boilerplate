@extends('layouts.app')

@section('content')
  @hasposts
    @posts
      @includeFirst(['partials.content-single-' . get_post_type(), 'partials.content-single'])
    @endposts
  @endhasposts
@endsection
