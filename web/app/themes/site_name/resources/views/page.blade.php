@extends('layouts.app')

@section('content')
  @hasposts
    @posts
      @includeFirst(['partials.content-page', 'partials.content'])
    @endposts
  @endhasposts
@endsection
