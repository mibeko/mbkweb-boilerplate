{{--
  Title: Carousel
  Category: common
  Icon: format-gallery
  Keywords: carousel slider
  Mode: edit
  Align: full
  SupportsAlign: full
  SupportsMode: edit
  SupportsMultiple: false
--}}

@hasfields('slides')
  <div id="carousel-{{ $block['id'] }}" class="{{ $block['classes'] }} swiper-container">
    <div class="swiper-wrapper fade-in">
      @fields('slides')
        <div class="swiper-slide carousel-slide">
          <div class="carousel-slide__inner">
            <div class="carousel-slide__bg parallax-bg" data-swiper-parallax-opacity="0.4">@image(get_sub_field('image'), 'full')</div>
            <div class="carousel-slide__fg">
              <div class="carousel-slide__header-spacer"></div>
              <div class="container" data-swiper-parallax="-100">
                  @hassub('content')
                    <div class="carousel-slide__content">
                      @sub('content')
                    </div>
                  @endsub
                  @hassub('link')
                    {!! App\button(get_sub_field('link'), 'carousel-slide__btn') !!}
                  @endsub
              </div>
            </div>
          </div>
        </div>
      @endfields
    </div>
    <div class="swiper-pagination"></div>
  </div>
@endhasfields
