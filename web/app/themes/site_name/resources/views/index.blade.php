@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @noposts
    <x-alert type="warning">
      {!! __('Sorry, no results were found.', '{{siteslug}}') !!}
    </x-alert>

    {!! get_search_form(false) !!}
  @endnoposts

  @hasposts
    @posts
      @includeFirst(['partials.content-' . get_post_type(), 'partials.content'])
    @endposts
  @endhasposts

  {!! get_the_posts_navigation() !!}
@endsection

@section('sidebar')
  @include('partials.sidebar')
@endsection
