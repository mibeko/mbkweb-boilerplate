<article @php(post_class())>
  <header>
    <h1 class="entry-title">
      {!! $title !!}
    </h1>

    @include('partials/entry-meta')
  </header>

  <div class="entry-content">
    @content
  </div>

  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', '{{siteslug}}'), 'after' => '</p></nav>']) !!}
  </footer>

  @php(comments_template())
</article>
