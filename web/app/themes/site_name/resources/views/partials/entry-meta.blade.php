<time class="updated" datetime="{{ get_post_time('c', true) }}">
  {{ get_the_date() }}
</time>

<p class="byline author vcard">
  <span>{{ __('By', '{{siteslug}}') }}</span>
  <a href="@authorurl" rel="author" class="fn">
    {{ get_the_author() }}
  </a>
</p>
